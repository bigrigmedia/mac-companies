<?php
/**
 * Plugin Name:    Mac companies portfolio
 * Plugin URI:
 * Description:     Create Mac Companies Portfolio Custom Post Type
 * Version:         1.0.0
 * Author:          Andrew Frankie Munoz
 * Author URI:
 * License:         MIT
 * License URI:     http://opensource.org/licenses/MIT
 */


// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

/**
 * @since 1.0.0
 *
 * @package MacCompaniesPortfolio
 * @author  Andrew Frankie Munoz
 */
class MacCompaniesPortfolio
{
    /**
     * Constructor
     *
     * @since 1.0.0
     */
    public function __construct()
    {
        // Variables
        $this->settings = [
            'dir'     => plugin_dir_url(__FILE__),
            'version' => '1.0.0'
        ];

        // Actions
        add_action('init', [$this, 'init']);

    }

    /**
     *  Create custom post type and taxonomies
     *
     * @link https://codex.wordpress.org/Function_Reference/register_post_type
     * @since 1.0.0
     */
    public function init()
    {
        // Custom post type items to show on the editor to include on your page
        register_post_type('portfolio', [
            'label'               => 'Portfolio',
            'public'              => true,
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'query_var'           => true,
            'rewrite'             => ['slug' => 'portfolio'],
            'capability_type'     => 'post',
            'has_archive'         => false,
            'hierarchical'        => false,
            'menu_position'       => null,
            'menu_icon'           => 'dashicons-tag',
            'supports'            => ['title','thumbnail','editor','excerpt']
        ]);
    }

    /**
     * Create custom alerts
     *
     * @param string $message
     * @param string $type
     * @since 1.0.0
     */
    public static function alert($message, $type)
    {
        return '<div class="c-alert c-alert--'.$type.'">'.$message.'</div>';
    }

    /**
     * Return projects output
     *
     * @since 1.0.0
     */
    public static function projects_markup()
    {
        global $post;

        // Set project title
        $title = get_the_title($post->ID);

        // Set project title
        $excerpts = get_the_excerpt($post->ID);

        // Set project featured image
        $image_featured = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'w757x498');

        // Set project placeholder image
        $image_placeholder = '/app/uploads/2017/03/Mac-companies.png';

        // Determine what image to use
        $image = has_post_thumbnail() ? $image_featured[0] : $image_placeholder;

        // Set project link
        $link = get_permalink($post->ID);

        //Square Feet intry
        $squarefeet = get_field('square_feet');


        // Project output
        $output =

                '<div class="o-container">
                    <div class="o-row">
                        <div class="o-col o-col--4@xs">
                            <a class="c-portfolio__link" href="'.$link.'"><img src="'.$image.'"></a>
                        </div>
                     <div class="o-col o-col--8@xs">
                        <a class="c-portfolio__tittle" href="'.$link.'"><h5 class="c-portfolio__title"><strong>Project Name/Location:</strong> '.$title.'</h5></a>
                        <p class="c-portfolio__footage"><strong>Square Foot:</strong> '.$squarefeet.'</p>
                        <p class="c-portfolio__excerpt"><strong>Info:</strong> '.$excerpts.'</p>
                     </div>
                   </div>
                </div>';

        // Return project output
        return $output;
    }

    /**
     * Return all project records
     *
     * @since 1.0.0
     */
    public static function all()
    {
        $query = new WP_Query([
            'post_type'         => 'portfolio',
            'posts_per_page'    => -1
        ]);

        if ($query->have_posts()) {
            $output = '<div class="c-portfolio">';

            while ($query->have_posts()) : $query->the_post();
                $output .= self::projects_markup();
            endwhile;
            wp_reset_postdata();

            $output .= '</div>';

            return $output;
        } else {
            self::alert('There are no records at this time.', 'error');
        }
    }
}

// Initialize main class
new MacCompaniesPortfolio();
