<?php
/**
 * Plugin Name:     Basic Testimonials
 * Plugin URI:      https://github.com/ernstoarllano/simple-testimonials
 * Description:     Create a basic testimonials carousel powered by Owl Carousel.
 * Version:         1.0.0
 * Author:          Ernesto Arellano Jr
 * Author URI:      
 * License:         MIT
 * License URI:     http://opensource.org/licenses/MIT
 */

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

/**
 * @since 1.0.0
 *
 * @package BasicTestimonials
 * @author  Ernesto Arellano Jr
 */
class BasicTestimonials
{
    /**
     * Constructor
     *
     * @since 1.0.0
     */
    public function __construct()
    {
        // Variables
        $this->settings = [
            'dir'     => plugin_dir_url(__FILE__),
            'version' => '1.0.0'
        ];

        // Actions
        add_action('init', [$this, 'init']);
        add_filter('manage_edit-testimonials_columns', [$this, 'admin_columns']);
        add_action('manage_testimonials_posts_custom_column', [$this, 'admin_columns_content'], 10, 2);
        add_filter('manage_edit-testimonials_sortable_columns', [$this, 'admin_sortable_columns']);
        //add_action('contextual_help', [$this, 'help'], 10, 3);
        add_action('wp_enqueue_scripts', [$this, 'styles']);
        add_action('wp_enqueue_scripts', [$this, 'scripts']);

        // Shortcode
        add_shortcode('testimonials', [$this, 'shortcode']);
    }

    /**
     *  Create custom post type and taxonomies
     *
     * @link https://codex.wordpress.org/Function_Reference/register_post_type
     * @link https://codex.wordpress.org/Function_Reference/register_taxonomy
     * @since 1.0.0
     */
    public function init()
    {
        // Create custom post type
        register_post_type('testimonials', [
          'label'               => 'Testimonials',
          'public'              => false,
          'publicly_queryable'  => false,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'query_var'           => false,
          'rewrite'             => ['slug' => 'testimonials'],
          'capability_type'     => 'post',
          'has_archive'         => false,
          'hierarchical'        => false,
          'menu_position'       => null,
          'menu_icon'           => 'dashicons-format-chat',
          'supports'            => ['title','editor']
        ]);

        // Creatue custom post type taxonomies
        register_taxonomy(
          'testimonials_source',
          'testimonials',
          [
            'label'         => __('Source'),
            'rewrite'       => ['slug' => 'testimonials-source'],
            'hierarchical'  => true
          ]
        );
    }

    /**
     * Modify admin columns
     *
     * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/manage_$post_type_posts_columns
     * @param array $columns
     * @since 1.0.0
     */
    public function admin_columns($columns)
    {
        $columns = [
            'cb'      => '<input type="checkbox" />',
    		'title'   => __('Title'),
    		'source'  => __('Source'),
    		'date'    => __('Date')
        ];

        return $columns;
    }

    /**
     * Populate admin columns
     *
     * @link https://codex.wordpress.org/Plugin_API/Action_Reference/manage_$post_type_posts_custom_column
     * @param string $column_name
     * @param int    $post_id
     * @since 1.0.0
     */
    public function admin_columns_content($column_name, $post_id)
    {
	global $post;
	    
        switch($column_name) {
            case 'source':
                $terms = get_the_terms($post_id, 'testimonials_source');

                if (!empty($terms)) {
                    $output = [];

                    foreach ($terms as $term) {
                        $output[] = sprintf(
                            '<a href="%s">%s</a>',
                            esc_url(add_query_arg([
                                'post_type' => $post->post_type,
                                'source'    => $term->slug
                            ], 'edit.php')),
                            esc_html(sanitize_term_field('name', $term->name, $term->term_id, 'source', 'display'))
                        );
                    }

                    echo join(', ', $output);
                }
            break;
            default:
            break;
        }
    }

    /**
     * Make admin columns sortable
     *
     * @link https://developer.wordpress.org/reference/hooks/manage_this-screen-id_sortable_columns/
     * @since 1.0.0
     */
    public function admin_sortable_columns()
    {
        $columns['source'] = 'source';

	    return $columns;
    }

    /**
     * Help documentation
     *
     * @link https://codex.wordpress.org/Adding_Contextual_Help_to_Administration_Menus
     * @param string $contextual_help
     * @param string $screen_id
     * @param object $screen
     * @since 1.0.0
     */
    public function help($contextual_help, $screen_id, $screen)
    {
        if ('testimonials' == $screen->id) {
            $contextual_help = '';
        } elseif ('edit-testimonials' == $screen->id) {
            $contextual_help = '';
        }

        return $contextual_help;
    }

    /**
     * Load carousel styles
     *
     * @link https://developer.wordpress.org/reference/functions/wp_enqueue_style/
     * @since 1.0.0
     */
    public function styles()
    {
        // Configure styles
        $styles = [
            'owl-carousel' => $this->settings['dir'] . 'assets/css/owl.carousel.min.css'
        ];

        // Enqueue styles
        foreach ($styles as $key => $style) {
            wp_enqueue_style($key, $style, false, $this->settings['version']);
        }
    }

    /**
     * Load carousel scripts
     *
     * @link https://developer.wordpress.org/reference/functions/wp_enqueue_script/
     * @since 1.0.0
     */
    public function scripts()
    {
        // Configure scripts
        $scripts = [
            'owl-carousel'          => $this->settings['dir'] . 'assets/js/owl.carousel.min.js',
            'simple-testimonials'   => $this->settings['dir'] . 'assets/js/scripts.js'
        ];

        // Enqueue scripts
        foreach ($scripts as $key => $script) {
            wp_enqueue_script($key, $script, ['jquery'], $this->settings['version'], true);
        }
    }

    /**
     * Create testimonials shortcode
     *
     * @link https://codex.wordpress.org/Shortcode_API
     * @param array $atts
     * @since 1.0.0
     */
    public function shortcode($atts)
    {
        // Set shortcode attributes
        $atts = shortcode_atts([
            'total' => 5
        ], $atts);

        // Get testimonial records
        $query = new WP_Query([
            'post_type'         => 'testimonials',
            'posts_per_page'    => $atts['total']
        ]);

        if ($query->have_posts()) {
            // Begin HTML output
            $html = '<div class="owl-carousel c-carousel c-carousel--testimonials">';

            // Create testimonials
            foreach ($query->posts as $testimonial) {
                // Format testimonal content
                $content = apply_filters('the_content', $testimonial->post_content);

                $html .= '<blockquote class="c-testimonial">
                            '.$content.'
                            <footer>
                                <cite>'.$testimonial->post_title.'</cite>
                            </footer>
                          </blockquote>';
            }

            // Reset post variable
            wp_reset_postdata();

            // Return HTML output
            return $html .= '</div>';
        }
    }
}

// Initialize main class
new BasicTestimonials();
