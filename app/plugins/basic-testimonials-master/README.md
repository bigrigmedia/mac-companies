# Basic Testimonials

A WordPress plugin to create a basic testimonials carousel powered by [Owl Carousel](http://www.owlcarousel.owlgraphic.com)

## Installation

### via WordPress Admin Panel

1. Download the [latest zip](https://github.com/ernstoarllano/basic-testimonials/releases/latest) of this repo.
2. In your WordPress admin panel, navigate to Plugins->Add New
3. Click Upload Plugin
4. Upload the zip file that you downloaded.

### Usage

All you need to do is activate the plugin and proceed with creating your testimonials. You can call the testimonial carousel anywhere by using the basic shortcode, eg:

`[testimonials]`

The above shortcode will output 5 testimonials by default, to modify the amount of testimonials you can pass a number into the total attribute, eg:

`[testimonials total=3]`
