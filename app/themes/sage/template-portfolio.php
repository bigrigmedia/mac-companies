<?php
/**
* Template Name: portfolio
*/

use Roots\Sage\Titles;

$greet = get_field('portfolio_field');

?>

<div class="c-content__header">
    <h1><?= Titles\title(); ?></h1>
    <hr />
</div>
<?php
echo $greet ;

if (method_exists('MacCompaniesPortfolio', 'all')) {
    echo MacCompaniesPortfolio::all();
}
