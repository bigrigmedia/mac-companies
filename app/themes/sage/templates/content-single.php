<?php while (have_posts()) : the_post(); ?>
    <?php if (!is_singular(['portfolio'])) { ?>
        <article <?php post_class('c-post c-post--single'); ?>>
            <header class="c-post__header">
                <h1 class="c-post__title"><?php the_title(); ?></h1>
                <?php get_template_part('templates/entry-meta'); ?>
            </header>
            <div class="c-post__content">
                <?php the_content(); ?>
            </div>
            <footer>
                <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
            </footer>
            <?php comments_template('/templates/comments.php'); ?>
        </article>
    <?php } elseif (is_singular('portfolio')) { ?>
        <?php get_template_part('templates/portfolio-single'); ?>
    <?php } elseif (is_singular('portfolio')) { ?>
        <?php get_template_part('templates/portfolio-single'); ?>
    <?php } ?>
<?php endwhile; ?>
