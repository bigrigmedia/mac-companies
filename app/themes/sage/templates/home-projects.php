<?php
//Set Variables
$projects_left = get_field('left_projects_field', 2, true);
$projects_right = get_field('right_content_field', 2, true);
?>
<div class="c-home c-home--projects">
    <div class="o-row">
        <div class="o-col o-col--12 o-col--6@md c-projects__left">
            <div class="o-row">
                <div class="o-col o-col--12">
                    <div class="o-col__inner">
                        <img src="<?php echo $projects_left ;?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="o-col o-col--12 o-col--6@md c-projects__right">
            <div class="o-row">
                <div class="o-col o-col--12">
                    <div class="o-col__inner">
                        <?php echo $projects_right ;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- o-row -->
