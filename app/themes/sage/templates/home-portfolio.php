<?php
//Set Variables
$portfolio_left = get_field('left_portfolio_field', 2, true);
$portfolio_right = get_field('right_content_field', 2, true);
?>
<div class="c-home c-home--portfolio">
    <div class="o-row">
        <div class="o-col o-col--12 o-col--6@md c-portfolio__left">
            <div class="o-row">
                <div class="o-col o-col--12">
                    <div class="o-col__inner">
                        <?php echo $portfolio_left ;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="o-col o-col--12 o-col--6@md c-portfolio__right">
            <div class="o-row">
                <div class="o-col o-col--12 c-portfolio__image--right" data-mobile="<?= get_field('right_image_field')['sizes']['w960x224'];?>" data-desktop="<?= get_field('right_image_field')['sizes']['w960x500'];?>">
                </div>
            </div>
        </div>
    </div>
</div><!-- o-row -->
