<?php
$squarefeet = get_field('square_feet');
 ?>

<div class="c-portfolio c-portfolio--single">
    <h1 class="c-portfolio__title u-text-center"><?php the_title(); ?></h1>
    <?php the_post_thumbnail(); ?>
    <p><strong>Square Feet:</strong> <?php echo $squarefeet ;?></p>
    <div class="c-post__content">
        <?php the_content(); ?>
    </div>
    <div class="c-portfolio__actions">
        <a class="c-portfolio__back" href="<?= get_permalink(16); ?>">&lsaquo; Back to portfolios</a>
    </div>

</div>
