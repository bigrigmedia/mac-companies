<?php
//Set Variables
$contact = get_field('contact_field', 2, true);
$googlemaps = get_field('mapiframe_field', 2, true);
$facebook = get_field('facebook_field', 2, true);
$twitter = get_field('twitter_field', 2, true);
$linkedin = get_field('linkedin_field', 2, true);
?>
<footer class="c-footer">
    <div class="o-container">
        <div class="o-row">
            <div class="o-col o-col--3@xs c-footer__nav">
                <h6>MAC COMPANIES</h6>
                <nav class="c-footer__menu">
                    <?php
                    if (has_nav_menu('footer_navigation')) :
                        wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'c-nav__list']);
                    endif;?>
                </nav>
                <div class="c-footer__social">
                  <div class="o-container">
                     <ul>
                        <?php if ($facebook) { ?>
                            <li>
                                <a href="<?php echo $facebook; ?>">
                                    <img src='/app/themes/sage/dist/images/facebookw.png'>
                                </a>
                            </li>
                            <?php } ?>
                            <?php if ($twitter) { ?>
                            <li>
                                <a href="<?php echo $twitter; ?>">
                                    <img src='/app/themes/sage/dist/images/twitterw.png'>
                                </a>
                            </li>
                            <?php } ?>
                            <?php if ($linkedin) { ?>
                            <li>
                                <a href="<?php echo $linkedin; ?>">
                                    <img src='/app/themes/sage/dist/images/linkedinw.png'>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
               </div>
            </div>
            <div class="o-col o-col--3@xs">
                <?php echo $contact ;?>
            </div>
            <div class="o-col o-col--6@xs">
                <?php echo $googlemaps ;?>
            </div>
        </div>
    </div>
    <div class="c-footer__rights">
        <div class="o-container">
            <p>©2017 Mac Companies | WEBSITE BY <a href="http://www.bigrigmedia.com/">Big Rig Media LLC ®</a></p>
        </div>
    </div>
</footer>
