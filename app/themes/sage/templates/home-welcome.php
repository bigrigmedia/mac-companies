<?php
//Set Variables
$welcome = get_field('welcome_field', 2, true);
?>
<div class="o-content c-welcome">
    <div class="o-container">
        <div class="o-row">
            <div class="o-col o-col--12@xs c-welcome__content">
                <?php echo $welcome;?>
            </div>
        </div><!-- o-row -->
    </div><!-- o-container -->
</div><!-- c-about-->
