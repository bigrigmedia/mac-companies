<?php
//Set Variables
$facebook = get_field('facebook_field', 2, true);
$twitter = get_field('twitter_field', 2, true);
$linkedin = get_field('linkedin_field', 2, true);
?>

<header class="c-header">
    <button class="c-header__toggle c-header__toggle--htx"><span></span></button>
    <div class="c-header__mobile">
        <nav class="c-header__menu">
            <?php
            if (has_nav_menu('mobile_navigation')) :
                wp_nav_menu(['theme_location' => 'mobile_navigation', 'menu_class' => 'c-nav__list']);
            endif;?>
        </nav>
    </div>
    <div class="c-header__container">
        <div class="c-header__social">
          <div class="o-container">
             <ul>
                <?php if ($facebook) { ?>
                    <li>
                        <a href="<?php echo $facebook; ?>">
                            <img src='/app/themes/sage/dist/images/facebook.png'>
                        </a>
                    </li>
                    <?php } ?>
                    <?php if ($twitter) { ?>
                    <li>
                        <a href="<?php echo $twitter; ?>">
                            <img src='/app/themes/sage/dist/images/twitter.png'>
                        </a>
                    </li>
                    <?php } ?>
                    <?php if ($linkedin) { ?>
                    <li>
                        <a href="<?php echo $linkedin; ?>">
                            <img src='/app/themes/sage/dist/images/linkedin.png'>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
       </div>
        <div class="o-container c-header__nav">
            <div class="o-row">
                <div class="o-col o-col--5@md c-header__nav--left">
                    <nav>
                        <?php
                        if (has_nav_menu('primary_navigation')) :
                            wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'c-nav__list']);
                        endif;
                        ?>
                    </nav>
                </div>
                <div class="o-col o-col--2@md">
                    <a  href="<?= esc_url(home_url('/')); ?>"> <img class="c-logo" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt=""/></a>
                </div>
                <div class="o-col o-col--5@md c-header__nav--right">
                    <nav>
                        <?php
                        if (has_nav_menu('secondary_navigation')) :
                            wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'c-nav__list']);
                        endif;
                        ?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
