<?php
//Set Variables
$manage = get_field('manage_field', 2, true);
?>
<div class="o-content c-manage">
    <div class="o-container">
        <div class="o-row">
            <div class="o-col o-col--12@xs c-manage__content">
                <?php echo $manage;?>
            </div>
        </div><!-- o-row -->
    </div><!-- o-container -->
</div><!-- c-about-->
