<?php
/**
* Template Name: Home
*/
get_template_part('templates/home-welcome');
get_template_part('templates/home-portfolio');
get_template_part('templates/home-projects');
get_template_part('templates/home-management');
