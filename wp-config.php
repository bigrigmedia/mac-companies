<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/**
 * Disable automatic update
 *
 * @link http://codex.wordpress.org/Configuring_Automatic_Background_Updates
 */
define('AUTOMATIC_UPDATER_DISABLED', true);

/**
 * Limit number of revisions saved
 *
 * @link https://codex.wordpress.org/Revisions
 */
define('WP_POST_REVISIONS', 10);

/**
 * Update default mem limit
 *
 * @link http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP
 */
define('WP_MEMORY_LIMIT', '64M');

/**
 * Disable dashboard file editing
 *
 * @link http://codex.wordpress.org/Hardening_WordPress#Disable_File_Editing
 */
define('DISALLOW_FILE_EDIT', true);

/**
 * WordPress Environment
 *
 * The default usage is:
 * 	development - For local development
 *	production - For live site
 */
define('WP_ENV', 'development');

// Switch MySQL settings based on environment
switch(WP_ENV){
	// Development
	case 'development':
		/** The name of the local database for WordPress */
		define('DB_NAME', 'Mac-manage_db');

		/** MySQL local database username */
		define('DB_USER', 'root');

		/** MySQL local database password */
		define('DB_PASSWORD', '');
	break;
	// Production
	case 'production':
		/** The name of the live database for WordPress */
		define('DB_NAME', 'live_database_name_here');

		/** MySQL live database username */
		define('DB_USER', 'live_username_here');

		/** MySQL live database password */
		define('DB_PASSWORD', 'live_password_here');
	break;
}

// ** MySQL settings - You can get this info from your web host ** //
/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Custom Content Directory **/
define('WP_CONTENT_DIR', dirname( __FILE__ ) . '/app');
define('WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/app');

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
 define('AUTH_KEY',         'Ac7@5h*,*vdQd@9BAaC2:^y!MY+!bm6F$NE#p8Z-rkd3>0B6U-h^!XjCW}foO_>[');
 define('SECURE_AUTH_KEY',  '1x7+BZ/U[lPwx,>Rd-UI[~sL)p9{H@c-pZJjPstYOBf|K?)QBca/W4U/gl9NQs2+');
 define('LOGGED_IN_KEY',    'pp$lNc>--Wok:|i4_TpLv=8{ 9kx`r{;GsWYO[^cQ=S$M$D9ly_]|17ZO2U7D68s');
 define('NONCE_KEY',        '8-=Et#*:9h)E6( =2w1j!l0LkEOH5lVX E5x|5J>=VhgnNQF?|X^y|#F[yn6D4C1');
 define('AUTH_SALT',        'ybU~,cM-`X0o^1Y1B2Np4JIV=-#$2]&/bxnu-%Z -_2xX.HXhFY%z~K&[JmuEctb');
 define('SECURE_AUTH_SALT', 'PaKv_Hp}MI5QkoF.{3qJ+p4[f+r`;Ud:&VD|eIZh^vo7K?s`LL,:M%(zy=Gj~zO@');
 define('LOGGED_IN_SALT',   'D{%9u?eKMUY!h c)D!,{gFc9LD0p!;l%^=rTOoc68#$RMQ(#C8{|@neth7xxcy~M');
 define('NONCE_SALT',       't)JD)d<^YQHjUrAO/10Ijn!7}/Dyn0+p9Ss,/Wp01SZ(o5|H8|{jXFBx=#TE-u5N');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'mp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/** Enable theme debug mode **/
define('THEME_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/wp/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
